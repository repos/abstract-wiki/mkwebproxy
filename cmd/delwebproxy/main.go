package main

import (
	"fmt"
	"os"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	"gitlab.wikimedia.org/repos/cloud/cloud-vps/go-cloudvps/pkg/clients"
)

func main() {
	if len(os.Args) != 2 {
		displayUsage()
		os.Exit(1)
	}

	domain := os.Args[1]

	opts := gophercloud.AuthOptions{
		IdentityEndpoint:            os.Getenv("OS_AUTH_URL"),
		ApplicationCredentialID:     os.Getenv("OS_APPLICATION_CREDENTIAL_ID"),
		ApplicationCredentialSecret: os.Getenv("OS_APPLICATION_CREDENTIAL_SECRET"),
	}

	provider, err := openstack.AuthenticatedClient(opts)
	if err != nil {
		panic(err)
	}

	client, err := clients.NewWebProxyClient(provider, gophercloud.EndpointOpts{
		Region: os.Getenv("OS_REGION_NAME"),
	})
	if err != nil {
		panic(err)
	}

	originalProxyResult := client.FindProxy(domain)
	if originalProxyResult.Err != nil {
		panic(originalProxyResult.Err)
	}

	originalProxy, err := originalProxyResult.Proxy()
	if err != nil {
		panic(err)
	}

	result := client.DeleteProxy(&originalProxy)
	if result.Err != nil {
		panic(result.Err)
	}
	fmt.Println("Proxy deleted.")
}

func displayUsage() {
	fmt.Println(`
delwebproxy: deletes a web proxy on Wikimedia Cloud VPC

USAGE:

    delwebproxy FQDN 

PARAMETERS:

    FQDN       the full qualified domain of the web proxy, e.g. example.wmcloud.org

ENVIRONMENT VARIABLES:
	
(note: these can be populated by sourcing an openrc file with application credentials)

    OS_AUTH_URL                         the OpenStack Keystone endpoint

    OS_APPLICATION_CREDENTIAL_IDi       the OpenStack application credential ID

    OS_APPLICATION_CREDENTIAL_SECRET    the OpenStack application credential secret
	`)
}
