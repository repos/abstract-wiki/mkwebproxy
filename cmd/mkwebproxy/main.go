package main

import (
	"fmt"
	"os"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	"gitlab.wikimedia.org/repos/cloud/cloud-vps/go-cloudvps/pkg/clients"
	"gitlab.wikimedia.org/repos/cloud/cloud-vps/go-cloudvps/pkg/webproxy"
)

func main() {
	if len(os.Args) != 3 {
		displayUsage()
		os.Exit(1)
	}

	domain := os.Args[1]
	backend := os.Args[2]

	opts := gophercloud.AuthOptions{
		IdentityEndpoint:            os.Getenv("OS_AUTH_URL"),
		ApplicationCredentialID:     os.Getenv("OS_APPLICATION_CREDENTIAL_ID"),
		ApplicationCredentialSecret: os.Getenv("OS_APPLICATION_CREDENTIAL_SECRET"),
	}

	provider, err := openstack.AuthenticatedClient(opts)
	if err != nil {
		panic(err)
	}
	client, err := clients.NewWebProxyClient(provider, gophercloud.EndpointOpts{
		Region: os.Getenv("OS_REGION_NAME"),
	})
	if err != nil {
		panic(err)
	}
	result := client.CreateProxy(&webproxy.Proxy{
		Domain:   domain,
		Backends: []string{backend},
	})
	if result.Err != nil {
		panic(result.Err)
	}
	fmt.Println("Proxy created.")
}

func displayUsage() {
	fmt.Println(`
mkwebproxy: makes a web proxy on Wikimedia Cloud VPC

USAGE:

    mkwebproxy FQDN BACKEND

PARAMETERS:

    FQDN       the full qualified domain of the web proxy, e.g. example.wmcloud.org

    BACKEND    the URL instance the web proxy directs, e.g. http://172.16.4.210:80 

ENVIRONMENT VARIABLES:
	
(note: these can be populated by sourcing an openrc file with application credentials)

    OS_AUTH_URL                         the OpenStack Keystone endpoint

    OS_APPLICATION_CREDENTIAL_IDi       the OpenStack application credential ID

    OS_APPLICATION_CREDENTIAL_SECRET    the OpenStack application credential secret
	`)
}
