# mkwebproxy

CLIs to create and/or delete [Web Proxies] in [Wikimedia Cloud VPS] using application credentials.

## Building

Requires
- [go], version 1.18+
- just (for running the build script)

```bash
just build
```

## Usage

1. create [application credentials] with project administrator rights
1. download the `-openrc.sh` file for your application credential

```bash
source app-cred-APP_CRED_NAME-openrc.sh  # the -openrc.sh file you just downloaded
./mkwebproxy PROXY_SUB_DOMAIN.wmcloud.org BACK_END_INSTANCE_NAME
./delwebproxy PROXY_SUB_DOMAIN.wmcloud.org
```

[go]: https://go.dev/doc/install
[Web Proxies]: https://wikitech.wikimedia.org/wiki/Help:Using_a_web_proxy_to_reach_Cloud_VPS_servers_from_the_internet
[Wikimedia Cloud VPS]: https://wikitech.wikimedia.org/wiki/Portal:Cloud_VPS
[application credentials]: https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/14/html/users_and_identity_management_guide/application_credentials
