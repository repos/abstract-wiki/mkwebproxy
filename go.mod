module gitlab.wikimedia.org/repos/abstract-wiki/mkwebproxy

go 1.18

require (
	github.com/gophercloud/gophercloud v1.1.1
	gitlab.wikimedia.org/repos/cloud/cloud-vps/go-cloudvps v0.1.1
)
